FROM node:14

WORKDIR /app

RUN npm i -g yarn --force

COPY package*.json ./

RUN yarn

COPY . .

RUN yarn

EXPOSE 3000

WORKDIR /app/packages/server

CMD [ "yarn", "start" ]