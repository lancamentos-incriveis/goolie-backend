module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        targets: {
          node: 'current',
        },
      },
    ],
    '@babel/preset-typescript',
  ],
  plugins: [
    ['@babel/transform-runtime'],
    [
      'module-resolver',
      {
        alias: {
          '@goolie/application-dal': './packages/dal/src',
          '@goolie/application-services': './packages/services/src',
          '@goolie/application-models': './packages/models/src',
          '@goolie/application-server': './packages/server/src',
        },
      },
    ],
  ],
};
