# Goolie backend project

## model schema

---

<br/>

![model schema](.drawio/model.png)

## Project relation architecture

---

<br/>

![project architecture](.drawio/project-relation.png)

## Pre requisites

- [node 14+](https://nodejs.org)
- [yarn](https://www.npmjs.com/package/yarn)
- one postgres instance to access

## Running project

---

### Config environment settings

<br/>
first of all you need to configure your environment settings.
 - Duplicate `.env.example` file inside `packages/server` folder and renameto .env
 - fill this environment variables with your postgres connection informations

### Running server

<br/>

Just run in root project folder
`yarn start`

## Deploying

---

<br/>
first of all you need to have a heroku app created (its free!).

Then you need to follow [heroku registry tutorial](https://dashboard.heroku.com/apps/goolie-backend/deploy/heroku-container) to deploy.

> note: you need to configure same environment variables as discribed inside `.env.example` file in your remote server

<br/>
<br/>
<br/>
<br/>

# REST API

The REST API is described below.

## Get list of Things

<br/>

### Request

`GET /api/v1/real-estate/:id`

`curl -i -H 'Accept: application/json' http://yourserver/api/v1/real-estate/1`

### Response

    HTTP/1.1 200 OK
    Date: Thu, 24 Feb 2011 12:36:30 GMT
    Status: 200 OK
    Connection: close
    Content-Type: application/json

```
    {
      "id": 1,
      "title": "apt 3 quartos na freguesia",
      "bathrooms": 3,
      "bedrooms": 1,
      "description": "ótimo apt na freguesia com 3 quartos e dependencia. imperdível!",
      "price": "60.000.000",
      "address": "Rua rua blablablablaal"
    }
```

## Create a new Real Estate

<br/>

### Request

`POST /api/v1/real-estate`

```
curl --location --request POST 'https://yourserver/api/v1/real-estate' \
--header 'Content-Type: application/json' \
--data-raw '{
  "title": "apt 3 quartos na freguesia",
  "description": "ótimo apt na freguesia com 3 quartos e dependencia. imperdível!",
  "bathrooms": 3,
  "bedrooms": 1,
  "price": "R$ 600.000,00",
  "address": "Rua rua blablablablaal"
}'
```

<br/>

### Response

    HTTP/1.1 201 Created
    Date: Thu, 24 Feb 2011 12:36:30 GMT
    Status: 201 Created
    Connection: close
    Content-Type: application/json

### Update a Real Estate

<br/>

`PUT /api/v1/real-estate/:id`

```
curl --location --request PUT 'https://yourserver/api/v1/real-estate/1' \
--header 'Content-Type: application/json' \
--data-raw '{
  "title": "apt 3 quartos na freguesia",
  "description": "ótimo apt na freguesia com 3 quartos e dependencia. imperdível!",
  "bathrooms": 3,
  "bedrooms": 1,
  "price": "R$ 600.000,00",
  "address": "Rua rua blablablablaal"
}'
```

<br/>

### Response

    HTTP/1.1 200 OK
    Date: Thu, 24 Feb 2011 12:36:30 GMT
    Status: 201 Created
    Connection: close
    Content-Type: application/json

```
    {
      "id": 1,
      "title": "apt 3 quartos na freguesia",
      "bathrooms": 3,
      "bedrooms": 1,
      "description": "ótimo apt na freguesia com 3 quartos e dependencia. imperdível!",
      "price": "60.000.000",
      "address": "Rua rua blablablablaal"
    }
```

### Delete a Real Estate

<br/>

`DELETE /api/v1/real-estate/:id`

```
curl --location --request DELETE 'https://yourserver/api/v1/real-estate/1' \
--header 'Content-Type: application/json' \
--data-raw '{
  "title": "apt 3 quartos na freguesia",
  "description": "ótimo apt na freguesia com 3 quartos e dependencia. imperdível!",
  "bathrooms": 3,
  "bedrooms": 1,
  "price": "R$ 600.000,00",
  "address": "Rua rua blablablablaal"
}'
```

<br/>

### Response

    HTTP/1.1 204 No Content
    Date: Thu, 24 Feb 2011 12:36:30 GMT
    Status: 201 Created
    Connection: close
    Content-Type: application/json

### Find Real Estates by parameters

<br/>

`GET /api/v1/real-estate`

Available Parameters:

`bathrooms: number`

`bedrooms: number`

`pricemin: number`

`pricemax: number`

```
curl --location --request GET 'https://yourserver/api/v1/real-estate?bathrooms=1&bedrooms=2&pricemin=600000&pricemax=700000' \
--header 'Content-Type: application/json'
```

<br/>

### Response

    HTTP/1.1 200 OK
    Date: Thu, 24 Feb 2011 12:36:30 GMT
    Status: 201 Created
    Connection: close
    Content-Type: application/json

```
[
    {
      "id": 1,
      "title": "apt 3 quartos na freguesia",
      "bathrooms": 3,
      "bedrooms": 1,
      "description": "ótimo apt na freguesia com 3 quartos e dependencia. imperdível!",
      "price": "60.000.000",
      "address": "Rua rua blablablablaal"
    },
    {
      "id": 2,
      "title": "apt 2 quartos na freguesia",
      "bathrooms": 2,
      "bedrooms": 1,
      "description": "ótimo apt na freguesia com 2 quartos e dependencia. imperdível!",
      "price": "700.000",
      "address": "Rua rua joao penha"
    }
]
```
