export class ValidationError extends Error {
  errors: string[];

  constructor(message: string, errors: string[]) {
    super(message);
    this.errors = errors;

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, ValidationError.prototype);
  }
}

export class NotFoundError extends Error { }