// import { Address } from './address';
import { Image } from './image';

export interface RealEstate {
  id: number;
  title: string;
  description: string;
  bathrooms: number;
  bedrooms: number;
  price: number;
  address: string;
  images?: Image[];
}