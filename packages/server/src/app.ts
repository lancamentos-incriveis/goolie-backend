import * as express from "express";
import apiRoutes from "./routes";

interface App {
  createApp(): express.Express;
}

export const createApp = (): express.Express => {
  const app = express();

  app.use(express.json())
  app.use(express.urlencoded({ extended: true }));

  app.use('/api/v1', apiRoutes);

  return app;
}