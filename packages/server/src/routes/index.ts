import { Router } from 'express'
import realEstateRouter from './realEstate';

const router = Router();

router.use('/real-estate', realEstateRouter);

export default router; 