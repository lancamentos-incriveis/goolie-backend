import { Router, Request, Response } from 'express';
import * as service from '@goolie/application-services/src/realEstate/service';
import { RealEstateInputDTO, GetAllRealEstateFilter, RangeFilter } from '@goolie/application-services/src/realEstate/types';
import { ValidationError, NotFoundError } from '@goolie/application-models/src/errors';
import isArray from 'lodash/isArray';

const realEstateRouter = Router()

const handle = async (fn: Function, res: Response): Promise<Response> => {
  try {
    return await fn();
  }
  catch (err) {
    console.error(err);

    if (err instanceof ValidationError) {
      return res.status(412).send(err);
    }
    else if (err instanceof NotFoundError) {
      return res.status(404).send(err.message);
    }
    else if (err instanceof Error) {
      return res.status(500).send(err);
    }
  }
}

realEstateRouter.get('/:id', async (req: Request, res: Response) => {
  const id = Number(req.params.id);

  handle(async () => {
    const result = await service.getById(id);
    return res.status(200).send(result);
  }, res);

})

realEstateRouter.put('/:id', async (req: Request, res: Response) => {
  const id = Number(req.params.id)
  const payload: RealEstateInputDTO = req.body

  payload.id = id;

  handle(async () => {
    const result = await service.update(payload);

    return res.status(200).send(result)
  }, res);
})

realEstateRouter.delete('/:id', async (req: Request, res: Response) => {
  const id = Number(req.params.id);

  handle(async () => {
    const result = await service.deleteById(id);
    return res.status(204).send({
      success: result
    })
  }, res);
})

realEstateRouter.post('/', async (req: Request, res: Response) => {
  const payload: RealEstateInputDTO = req.body;

  handle(async () => {
    const result = await service.create(payload);
    return res.status(201).send(result);
  }, res);
})

realEstateRouter.get('/', async (req: Request, res: Response) => {
  const { bedrooms, bathrooms, pricemin, pricemax } = req.query;

  const filters: GetAllRealEstateFilter = {
    bedrooms: bedrooms ? Number(bedrooms) : undefined,
    bathrooms: bathrooms ? Number(bathrooms) : undefined,
    price: {
      gte: pricemin ? Number(pricemin) : undefined,
      lte: pricemax ? Number(pricemax) : undefined
    },
  }

  handle(async () => {
    const results = await service.getAll(filters);
    return res.status(200).send(results);
  }, res);
})
export default realEstateRouter 