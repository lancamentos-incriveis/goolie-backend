import * as dotenv from 'dotenv';
dotenv.config();

import { createApp } from "./app";

import { initDbConnection } from '@goolie/application-dal/src/db';


const PORT = process.env.PORT || 3000;

const serverInstance = createApp();

const test = async () => {
  await initDbConnection({
    user: process.env.POSTGRESDB_USER,
    pass: process.env.POSTGRESDB_PASS,
    host: process.env.POSTGRESDB_HOST,
    port: process.env.POSTGRESDB_PORT,
    dbname: process.env.POSTGRESDB_DBNAME,
    logging: process.env.NODE_ENV === 'development',
    alter: process.env.NODE_ENV === 'development',
  });
}

try {
  serverInstance.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`));
  test();
} catch (error) {
  console.log(`Error occurred: ${error.message}`);
}