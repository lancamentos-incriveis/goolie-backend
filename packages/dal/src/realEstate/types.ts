import { Optional } from 'sequelize';
import { RealEstate as RealEstateAttributes } from '@goolie/application-models';

export type RangeFilter = {
  lte?: number;
  gte?: number;
}

export type GetAllRealEstateFilter = {
  bedrooms?: number,
  bathrooms?: number,
  price?: RangeFilter
}

export interface RealEstateInput extends Optional<RealEstateAttributes, 'id'> { }
export interface RealEstateOutput extends Required<RealEstateAttributes> { }