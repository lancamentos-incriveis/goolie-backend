import { DataTypes, Model, Sequelize } from 'sequelize'
import { RealEstate as RealEstateAttributes } from '@goolie/application-models';
import { RealEstateInput } from './types';
import Image from '../image/model';

export class RealEstate extends Model<RealEstateAttributes, RealEstateInput> implements RealEstateAttributes {
  id: number;
  title: string;
  description: string;
  bathrooms: number;
  bedrooms: number;
  price: number;
  address: string;
  images: Image[];
  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
  public readonly deletedAt!: Date;
}

export const init = (instance: Sequelize) => {
  RealEstate.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    bedrooms: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    bathrooms: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    price: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    address: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    timestamps: true,
    sequelize: instance,
    paranoid: true,
    modelName: 'RealEstates'
  })

  RealEstate.hasMany(Image);
}


export default RealEstate