import { Op } from "sequelize";
import { GetAllRealEstateFilter } from './types'
import { RealEstate } from './model'
import { RealEstateInput, RealEstateOutput } from './types';
import { NotFoundError } from '@goolie/application-models/src/errors'

export const create = async (payload: RealEstateInput): Promise<RealEstateOutput> => {
  const data = await RealEstate.create(payload);

  return data;
}

export const update = async (id: number, payload: Partial<RealEstateInput>): Promise<RealEstateOutput> => {
  const data = await RealEstate.findByPk(id);
  if (!data) {
    // @todo throw custom error
    throw new NotFoundError('not found');
  }

  const updated = await (data as RealEstate).update(payload);

  return updated;
}

export const getById = async (id: number): Promise<RealEstateOutput> => {
  const data = await RealEstate.findByPk(id);
  if (!data) {
    // @todo throw custom error
    throw new NotFoundError('not found');
  }

  return data;
}

export const deleteById = async (id: number): Promise<boolean> => {
  const deletedCount = await RealEstate.destroy({
    where: { id }
  });
  return !!deletedCount;
}

export const getAll = async (filters: GetAllRealEstateFilter): Promise<RealEstateOutput[]> => {
  const conditions = {};

  console.log('filters', filters)

  if (filters.bathrooms >= 0) conditions['bathrooms'] = { [Op.eq]: filters.bathrooms };
  if (filters.bedrooms >= 0) conditions['bedrooms'] = { [Op.eq]: filters.bedrooms };
  if (filters.price.lte >= 0 || filters.price.gte >= 0) {
    const rangeCondition = {};
    if (filters.price.lte) rangeCondition[Op.lte] = filters.price.lte;
    if (filters.price.gte) rangeCondition[Op.gte] = filters.price.gte;

    conditions['price'] = rangeCondition;
  }

  console.log('filtered filters', filters)


  return RealEstate.findAll({ where: conditions });
}