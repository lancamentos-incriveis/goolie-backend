import { GetAllImageFilter } from './types';
import { Image } from './model';
import { ImageInput, ImageOutput } from './types';
import { NotFoundError } from '@goolie/application-models/src/errors';

export const create = async (payload: ImageInput): Promise<ImageOutput> => {
  const data = await Image.create(payload);
  return data;
}

export const update = async (id: number, payload: Partial<ImageInput>): Promise<ImageOutput> => {
  const data = await Image.findByPk(id);
  if (!data) {
    // @todo throw custom error
    throw new NotFoundError('not found');
  }
  const updateddata = await (data as Image).update(payload);

  return updateddata
}

export const getById = async (id: number): Promise<ImageOutput> => {
  const data = await Image.findByPk(id);
  if (!data) {
    // @todo throw custom error
    throw new Error('not found');
  }
  return data
}

export const deleteById = async (id: number): Promise<boolean> => {
  const deletedCount = await Image.destroy({
    where: { id }
  });

  return !!deletedCount;
}

export const getAll = async (filters?: GetAllImageFilter): Promise<ImageOutput[]> => {
  return Image.findAll({});
}