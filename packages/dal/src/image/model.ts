import { DataTypes, Model, Sequelize } from 'sequelize'
import { Image as ImageAttributes } from '@goolie/application-models';
import { ImageInput } from './types';

export class Image extends Model<ImageAttributes, ImageInput> implements ImageAttributes {
  id: number;
  title: string;
  url: string;
  isCover: boolean;
  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
  public readonly deletedAt!: Date;
}

export const init = (instance: Sequelize) => {
  Image.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false
    },
    isCover: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    timestamps: true,
    sequelize: instance,
    paranoid: true,
    modelName: 'Images'
  })
}


export default Image