import { Optional } from 'sequelize';
import { Image as ImageAttributes } from '@goolie/application-models';

export type GetAllImageFilter = {}

export interface ImageInput extends Optional<ImageAttributes, 'id'> { }
export interface ImageOutput extends Required<ImageAttributes> { }