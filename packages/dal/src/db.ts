import { Sequelize, Options } from 'sequelize';
import { init as realEstateInit } from './realEstate/model';
import { init as imageInit } from './image/model';

interface DbConnection { }

interface PostgresConnection extends DbConnection {
  createConnection(conn: PostgresConnection): void;
}

export type PostgresConnectionParams = {
  user: string;
  pass: string;
  host: string;
  port: string;
  dbname: string;
  logging: boolean;
  alter: boolean;
}


export const initDbConnection = async (conn: PostgresConnectionParams) => {
  const options: Options = {}

  if (conn.logging) options.logging = (...msg) => console.log(msg)

  const sequelize = new Sequelize(`postgres://${conn.user}:${conn.pass}@${conn.host}:${conn.port}/${conn.dbname}`, {
    dialect: 'postgres',
    dialectOptions: {
      ssl: {
        require: true,
        rejectUnauthorized: false,
      }
    }
  });

  try {
    await sequelize.authenticate();
    //init all models
    imageInit(sequelize);
    realEstateInit(sequelize);

    //sync all models
    await sequelize.sync({ alter: conn.alter });

    return;
  } catch (error) {
    console.error('Unable to connect to the database:', error);

    throw error;
  }
}


