import * as dal from '@goolie/application-dal/src/realEstate/dal';
import { RealEstateInput as RealEstateDALInput, RealEstateOutput as RealEstateDALOutput } from '@goolie/application-dal/src/realEstate/types';
import { RealEstateInputDTO, RealEstateOutputDTO, GetAllRealEstateFilter } from './types';
import { validate, validateId } from './validator';

const parseDalOutputToOutput = (payload: RealEstateDALOutput): RealEstateOutputDTO => {

  return {
    id: payload.id,
    title: payload.title,
    bathrooms: payload.bathrooms,
    bedrooms: payload.bedrooms,
    description: payload.description,
    price: payload.price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'),
    address: payload.address,
    images: payload.images,
  }
}


const parsePaylaodToDalInput = (payload: RealEstateInputDTO): RealEstateDALInput => {

  return {
    id: payload.id,
    title: payload.title,
    bathrooms: payload.bathrooms,
    bedrooms: payload.bedrooms,
    description: payload.description,
    price: Number(payload.price.replace(/\D/g, '')),
    address: payload.address,
    images: [],
  }
}

export const create = async (payload: RealEstateInputDTO): Promise<RealEstateOutputDTO> => {
  validate(payload);

  const result = await dal.create(parsePaylaodToDalInput(payload));

  return parseDalOutputToOutput(result);
}

export const update = async (payload: RealEstateInputDTO): Promise<RealEstateOutputDTO> => {
  validateId(payload.id);
  validate(payload);

  const dalInput = parsePaylaodToDalInput(payload);

  console.log(dalInput)

  const updated = await dal.update(dalInput.id, dalInput);

  return parseDalOutputToOutput(updated);
}

export const getById = async (id: number): Promise<RealEstateOutputDTO> => {
  validateId(id);

  const element = await dal.getById(id);

  return parseDalOutputToOutput(element);
}

export const deleteById = async (id: number): Promise<boolean> => {
  validateId(id);

  const deleted = await dal.deleteById(id);

  return deleted;
}

export const getAll = async (filters: GetAllRealEstateFilter): Promise<RealEstateOutputDTO[]> => {
  const results = await dal.getAll(filters);

  return results.map(parseDalOutputToOutput);
}