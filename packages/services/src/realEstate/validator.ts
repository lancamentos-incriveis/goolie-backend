import * as Validator from 'validatorjs';
import { ValidationError } from '@goolie/application-models/src/errors';
import { RealEstateInputDTO } from './types';

export const validate = (input: RealEstateInputDTO): void => {
  const validator = new Validator(input, {
    id: 'min:0',
    title: 'required',
    description: 'required',
    bathrooms: 'min:1',
    bedrooms: 'min:1',
    price: ['required', 'regex:^\s*(?:[1-9]\d{0,2}(?:\.\d{3})*|0)(?:,\d{1,2})?$'],
    address: 'required',
  });

  if (!validator.passes()) {
    throw new ValidationError(`${Object.keys(validator.errors.all()).length} errors`, validator.errors.all());
  }

  return;
}

export const validateId = (input: number): void => {
  const validator = new Validator({ id: input }, {
    id: 'min:0'
  });

  if (!validator.passes()) {
    throw new ValidationError(`${Validator.errors.errorCount} errors`, validator.errors.all());
  }

  return;
}