export type RealEstateInputDTO = {
  id?: number;
  title: string;
  description: string;
  bathrooms: number;
  bedrooms: number;
  price: string;
  address: string;
}

export type RealEstateOutputDTO = {
  id?: number;
  title: string;
  description: string;
  bathrooms: number;
  bedrooms: number;
  price: string;
  address: string;
  images: any[];
}

export type RangeFilter = {
  lte?: number;
  gte?: number;
}

export type GetAllRealEstateFilter = {
  bedrooms?: number,
  bathrooms?: number,
  price?: RangeFilter
}